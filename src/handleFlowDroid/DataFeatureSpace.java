package handleFlowDroid;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataFeatureSpace {
	List<int[]> dataFeatureSpace;
	ArrayList<String> listSrc = new ArrayList<String> (Arrays.asList("NO_CATEGORY", "CONTACT_INFORMATION", "NETWORK_INFORMATION", "SYSTEM_SETTINGS",
			"CALENDAR_INFORMATION", "FILE_INFORMATION", "LOCATION_INFORMATION", "IMAGE", "NFC", "SMS_MMS", "PHONE_STATE", "BROWSER_INFORMATION",
			"BLUETOOTH_INFORMATION", "UNIQUE_IDENTIFIER", "ACCOUNT_INFORMATION", "DATABASE_INFORMATION", "BUNDLE", "IPC", "AUDIO", "WIDGET"));
	ArrayList<String> listSnk = new ArrayList<String> (Arrays.asList("NO_CATEGORY", "CONTACT_INFORMATION", "SYSTEM_SETTINGS", "EMAIL", "NETWORK", "FILE", 
			"CALENDAR_INFORMATION", "BLUETOOTH", "LOG", "SYNCHRONIZATION_DATA", "IMAGE", "NFC", "SMS_MMS", "BROWSER_INFORMATION", "ACCOUNT_SETTINGS",
			"IPC", "AUDIO", "PHONE_CONNECTION", "VOIP", "VIDEO","DATABASE_INFORMATION"));
	
	ArrayList<String> appNames;
	ArrayList<String> recordClasses;
	
	public boolean valueAdded;
	
	public DataFeatureSpace(){
		appNames = new ArrayList<String>();
		recordClasses = new ArrayList<String>();
		
		int[] dataRow = new int[listSrc.size()*listSnk.size()];
		dataFeatureSpace = new ArrayList<int[]>();
		//dataFeatureSpace.add(dataRow);
		valueAdded = false;
	}

	public List<int[]> getDataFeatureSpace(){
		return dataFeatureSpace;
	}
	
	public void readFromFile(String filename){
		// Reading input file
		File file = new File(filename);
		if(file.exists() && !file.isDirectory()) {
			try {
				BufferedReader in
				= new BufferedReader(new FileReader(file));
				String line = null;				 
				while((line =in.readLine())!=null && !line.contains("@data")){				 
				}
				while((line =in.readLine())!=null){
					String[] linecontent = line.split(",");
					addEmptyRecord(linecontent[0], linecontent[linecontent.length-1]);
					for (int i=1; i<linecontent.length-1;i++){
						dataFeatureSpace.get(dataFeatureSpace.size()-1)[i-1] = Integer.parseInt(linecontent[i]);						
					}
				}
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
//		// Adding Contents
//		for (int i=0; i<appNames.size(); i++){
//			fileContent.append(appNames.get(i)+",");
//			for (int j=0; j<dataFeatureSpace.get(0).length; j++){
//				fileContent.append(Integer.toString(dataFeatureSpace.get(i)[j]) + ",");
//			}
//			fileContent.append(recordClasses.get(i)+newline);
//		}
//		//Writing to file		
//		try {
//			FileWriter fw;
//			fw = new FileWriter(file.getAbsoluteFile());
//			BufferedWriter bw = new BufferedWriter(fw);
//			bw.write(fileContent.toString());
//			bw.close();	
//
//			appNames.clear();
//			dataFeatureSpace.clear();
//			recordClasses.clear();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}	
	}

	public void produceOutputFile(String filename){
		// Defining Newline
		String newline = System.getProperty("line.separator");
		// Creating output file
		File file = new File(filename);
		boolean fileExists = false;
		StringBuilder fileContent;
		if(!(file.exists() && !file.isDirectory())) {
			fileExists = true;
			// Defining attributes
			fileContent =  new StringBuilder("@relation APK_Features"+newline);
			fileContent.append("@attribute AppName string"+newline);		
			for (int i=0; i<listSrc.size(); i++)
				for (int j=0; j<listSnk.size(); j++){
					fileContent.append("@attribute "+listSrc.get(i)+"____"+listSnk.get(j)+ " numeric"+newline);
				}
			// Defining class attribute
			fileContent.append("@attribute class {");
			ArrayList<String> classSet = new ArrayList<>();
			for (int i=0; i<recordClasses.size(); i++){
				if (!classSet.contains(recordClasses.get(i)))
					classSet.add(recordClasses.get(i));
			}
			for (int i=0; i<classSet.size(); i++){
//				fileContent.append("\"");
				fileContent.append(classSet.get(i));
//				fileContent.append("\"");
				if (i<classSet.size()-1)
					fileContent.append(",");
			}
			fileContent.append("}"+newline+"@data"+newline);
		}else{
			fileContent = new StringBuilder();
			 try {
				BufferedReader in
				   = new BufferedReader(new FileReader(file));
				String line = null;				 
				  while((line =in.readLine())!=null){				 
				  fileContent.append(line).append("\n");
				  }
				  in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// Adding Contents
		for (int i=0; i<appNames.size(); i++){
			fileContent.append("'");
			fileContent.append(appNames.get(i)+"',");
			for (int j=0; j<dataFeatureSpace.get(0).length; j++){
				fileContent.append(Integer.toString(dataFeatureSpace.get(i)[j]) + ",");
			}
			fileContent.append(recordClasses.get(i)+newline);
		}
		//Writing to file		
		try {
			FileWriter fw;
			fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(fileContent.toString());
			bw.close();	
			
			//appNames.clear();
			//dataFeatureSpace.clear();
			//recordClasses.clear();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void addEmptyRecord(String recordClass){
		int[] dataRow = new int[listSrc.size()*listSnk.size()];
		dataFeatureSpace.add(dataRow);
		recordClasses.add(recordClass);
	}
	public void addEmptyRecord(String appName, String recordClass){
		int[] dataRow = new int[listSrc.size()*listSnk.size()];
		dataFeatureSpace.add(dataRow);
		recordClasses.add(recordClass);
		appNames.add(appName);
	}
	
	public void addValue(String strSourceCat, String strSinkCat){
		int intSourceCatIx = listSrc.indexOf(strSourceCat);
		int intSinkCatIx = listSnk.indexOf(strSinkCat);
		if (intSinkCatIx == -1) {
			intSinkCatIx = listSnk.indexOf("NO_CATEGORY");
		}
		else if (intSourceCatIx == -1) {
			intSourceCatIx = listSrc.indexOf("NO_CATEGORY");
		}
		dataFeatureSpace.get(dataFeatureSpace.size()-1)[intSourceCatIx*listSnk.size()+intSinkCatIx] ++;
		valueAdded = true;
	}
}
