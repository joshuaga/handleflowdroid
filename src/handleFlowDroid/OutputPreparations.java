package handleFlowDroid;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class OutputPreparations {
	public static HashMap<String, String> getCategoryAPImap() {
		// TODO Auto-generated method stub
		
		HashMap<String, String> categoryApiHashMap = null;
		
		try {
			categoryApiHashMap = createCategoryAPImap();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
//		ArrayList<String> catList =  getListOfCategories(categoryApiHashMap);
//		for (int i=0; i<catList.size(); i++)
//			System.out.println(catList.get(i));
		return categoryApiHashMap;
	}
	

	private static HashMap<String, String> createCategoryAPImap() throws IOException{
		String fileName = "prmDomains.txt";
		String[][] map = new String[countLines(fileName)+1][2];
		int a = countLines(fileName);
		FileReader fileIn = new FileReader(fileName);
		BufferedReader bufferedReader = 
                new BufferedReader(fileIn);
		String line = null;
		String strTmp;
		Integer intLineNumber = 0;
        while((line = bufferedReader.readLine()) != null) {
        	if(!line.contains("%"))
        		continue;
        	strTmp = line.replaceAll("\\s","");
        	map[intLineNumber] = strTmp.split("\\%");
        	intLineNumber++;
        }
        bufferedReader.close();
        
        HashMap<String, String> hashMap = new HashMap<>();
        for (int i=0; i<map.length; i++){
        	hashMap.put(map[i][1], map[i][0]);
        }
        return hashMap;
	}
	

	private static int countLines(String filename) throws IOException {
	    InputStream is = new BufferedInputStream(new FileInputStream(filename));
	    try {
	        byte[] c = new byte[1024];
	        int count = 0;
	        int readChars = 0;
	        boolean empty = true;
	        while ((readChars = is.read(c)) != -1) {
	            empty = false;
	            for (int i = 0; i < readChars; ++i) {
	                if (c[i] == '\n') {
	                    ++count;
	                }
	            }
	        }
	        return (count == 0 && !empty) ? 1 : count;
	    } finally {
	        is.close();
	    }
	}
	

	public static ArrayList<String> getListOfCategories(){
		HashMap<String, String> categoryApiMap = null;
		
		try {
			categoryApiMap = createCategoryAPImap();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ArrayList<String> catList = new ArrayList<>();
		Collection<String> catCollection = categoryApiMap.values();
		catCollection.remove(null);
		Iterator<String> temp = catCollection.iterator();
		
		String strTmp = "";
		while (temp.hasNext()){
			strTmp = temp.next();
			if (!catList.contains(strTmp))
				catList.add(strTmp);
		}
		return catList;
	}
	
}
